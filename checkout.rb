require_relative 'promotional_rule'
class Checkout
  attr_reader :items, :promotional_rules
  def initialize(promotional_rules=::PromotionalRule.all)
    @promotional_rules = promotional_rules
    @items = []
  end

  def scan(item)
    items << item
  end


  def total
    total_so_far = total_without_promotions
    promotional_rules.each do |promotional_rule_class|
      promotional_rule = promotional_rule_class.new(items: items, total: total_so_far)
      total_so_far = total_so_far - promotional_rule.amount if promotional_rule.apply?
    end
    total_so_far.round
  end

  private

  def total_without_promotions
    items.map(&:price).reduce(:+)
  end
end