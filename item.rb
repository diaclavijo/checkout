class Item
  attr_reader :product_code, :name, :price

  def initialize(product_code:, name:, price:)
    @product_code = product_code
    @name = name
    @price = price
  end

  def self.lavender
    new(
      product_code: 1,
      name: 'Lavender heart',
      price: 925,
    )
  end

  def self.cufflinks
    new(
      product_code: 2,
      name: 'Personalised cufflinks',
      price: 4500,
    )
  end

  def self.kids
    new(
      product_code: 3,
      name: 'Kids T-shirt',
      price: 1995,
    )
  end
end