class PromotionalRule
  attr_reader :items, :total
  def initialize(items:, total:)
    @items = items
    @total = total
  end

  def self.all
    [
      TwoOrMoreLavenders,
      Over60,
    ]
  end

  class Over60 < PromotionalRule
    def apply?
      total > 6000
    end

    def amount
      0.10 * total
    end
  end

  class TwoOrMoreLavenders < PromotionalRule
    def apply?
      lavender_items_count > 1
    end

    def amount
      (lavender_item.price - 850) *  lavender_items_count
    end

    private

    def lavender_items_count
      items.count { |item| item.product_code == 1 }
    end

    def lavender_item
      items.find { |item| item.product_code == 1 }
    end
  end
end