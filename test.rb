require_relative 'checkout'
require_relative 'item'

def assert(condition, msg='Failed assertion')
  raise msg unless condition
end

def assert_equal(value1, value2, msg = nil)
  msg = "#{value1} and #{value2} expected to be equal but they are not"
  assert value1 == value2, msg
end


# Test 1
checkout = Checkout.new
checkout.scan(Item.lavender)
checkout.scan(Item.cufflinks)
checkout.scan(Item.kids)
assert_equal checkout.total, 6678, 'Failed test 1'

# Test 2
checkout = Checkout.new
checkout.scan(Item.lavender)
checkout.scan(Item.kids)
checkout.scan(Item.lavender)
assert_equal checkout.total, 3695, 'Failed test 2'

# Test 3
checkout = Checkout.new
checkout.scan(Item.lavender)
checkout.scan(Item.cufflinks)
checkout.scan(Item.lavender)
checkout.scan(Item.kids)
assert_equal checkout.total, 7376, 'Failed test 3'